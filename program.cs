using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            char again = 'Y';
            while (again == 'Y')
            {
                double a, b, total;
                char oper;
                Console.Write("Введите первое число: ");
                a = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите второе число: ");
                b = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите опертор: ");
                oper = Convert.ToChar(Console.ReadLine());
                if (oper == '+')
                {
                    total = a + b;
                    Console.WriteLine("Сумма этих двух чисел = " + total);
                }
                else if (oper == '-')
                {
                    total = a - b;
                    Console.WriteLine("Разность этих двух чисел = " + total);
                }
                else if (oper == '*')
                {
                    total = a * b;
                    Console.WriteLine("Результатом умножения этих двух чисел = " + total);
                }
                else if (oper == '/')
                {
                    total = a / b;
                    Console.WriteLine("Результатом деления этих двух чисел = " + total);
                }
                else Console.WriteLine("Неизвестный оператор.");
                Console.WriteLine("Вы хотите продолжить работу с калькулятором? (Y/N)");
                again = Convert.ToChar(Console.ReadLine());
            }

        }
    }
}
